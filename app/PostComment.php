<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    public function user(){
        return $this->belongsTo('App\Post');
    }
    public function comments (){
        return$this->hasMany('App\PostComment');
    }
}
